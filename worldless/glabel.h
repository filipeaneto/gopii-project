/**
 * @file  glabel.h
 * @brief  Declaração da classe gLable.
 * @author  Filipe Neto
 * @version 1.0
 * @date  Criação: 04/01/2012
 * @date  Alteração: 04/01/2012
 * @todo 
 *  @li Por enquanto nada.
 *
 *  Este arquivo contém a declaração da classe gLabel.
 */

#ifndef __G_LABEL__
#define __G_LABEL__

#include "orx.h"
#include "gobject.h"

class gLabel : public gObject
{

public:
    gLabel(const orxSTRING _zObjectSection, const orxSTRING _zString);
    ~gLabel();
    
    orxSTATUS SetString(const orxSTRING _zString);
    
    void Select();
};

#endif // __G_LABEL__
