/**
 * @file  
 * @brief  
 * @author Filipe Neto
 * @version 1.0
 * @date  Criação: 04/01/2012
 * @date  Alteração: 04/08/2012
 * @todo 
 *
 */

#include "genemy.h"

void gEnemy::Update()
{
    if(bDieOutOfScreen)
    {
        orxVECTOR vPos;
        GetPosition(&vPos);

        if(!orxAABox_IsInside(pstScreen, &vPos))
        {
            bIsDead = orxTRUE;
        }
    }
}

gEnemy::gEnemy(const orxSTRING _zSectionName, orxAABOX * _pstScreen) :
    gObject(_zSectionName, gOBJECT_ENEMY),
    pstScreen(_pstScreen),
    bIsDead(orxFALSE)
{
    if(orxConfig_PushSection(zSectionName))
    {
        u32Life = orxConfig_GetU32("Life");
        fLifeTime = orxConfig_GetFloat("LifeTime");
        bDieOutOfScreen = orxConfig_GetBool("DieOutOfScreen");

        // Por segurança
        orxObject_SetLifeTime(pstObject, -1.0f);

        orxConfig_PopSection();
    }
}

gEnemy::~gEnemy()
{

}

void gEnemy::Select()
{
}

void gEnemy::Kill()
{
    bIsDead = orxTRUE;
}

orxBOOL gEnemy::IsDead()
{
    return bIsDead;
}

