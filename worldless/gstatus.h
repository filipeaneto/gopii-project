/**
 * @file  
 * @brief  
 * @author Filipe Neto
 * @version 1.0
 * @date  Criação: 03/25/2012
 * @date  Alteração: 04/08/2012
 * @todo 
 *
 */

#ifndef __G_STATUS__
#define __G_STATUS__

#include "orx.h"

/**
 * @class __gStatus_t
 * @author Filipe Neto
 * @date 03/25/2012
 * @file gstatus.h
 * @brief 
 */
struct __gStatus_t
{
    __gStatus_t() :
        u32TopScore(0)
    {
    }

    __gStatus_t(const orxSTRING _zSectionName) :
        zSectionName(_zSectionName)
    {

        if(orxConfig_PushSection(zSectionName))
        {
            u32TopScore = orxConfig_GetU32("TopScore");

            orxConfig_PopSection();
        }
    }

    ~__gStatus_t()
    {
    }

    // Game attributes
    const orxSTRING zPlayer;
    const orxSTRING zModule;
    const orxSTRING zSaveFile;

    // Info attributes
    const orxSTRING zSectionName;

    // Save attributes
    orxU32 u32TopScore;
};

typedef struct __gStatus_t gStatus;

#endif // __G_STATUS__
