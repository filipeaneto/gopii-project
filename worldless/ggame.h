/**
 * @file  ggame.h
 * @brief  Declaração da classe gGame.
 * @author  Filipe Neto
 * @version 1.0
 * @date  Criação: 03/25/2012
 * @date  Alteração: 03/25/2012
 * @todo 
 *  @li Nada :)
 *
 *  Este arquivo contém a declaração da class gGame.
 */

#ifndef __G_GAME_H__
#define __G_GAME_H__

#include "orx.h"
#include "gstatus.h"
#include "gquick.h"

/**
 * @class gGame
 * @author Filipe Neto
 * @date 03/25/2012
 * @file ggame.h
 * @brief Classe que descreve o jogo.
 * 
 *  Esta classe contem métodos estáticos para inicializar, rodar e
 *  finalizar o jogo (singleton).
 */
class gGame
{
public:
    /**
     * @brief Acessa o singleton.
     * @return Instância da classe gGame.
     */
    static gGame * Instance();

    /**
     * @brief Manipula eventos de módulos.
     * @param _pEvent Evento recebido.
     */
    static orxSTATUS orxFASTCALL EventHandler(const orxEVENT* _pEvent);

    /**
     * @brief Chama rotinas para a inicialização do jogo.
     */
    static orxSTATUS orxFASTCALL Init();

    /**
     * @brief Chama rotinas para a finalização do jogo.
     */
    static void      orxFASTCALL Exit();
    
    /**
     * @brief Chama rotinas para o funcionamento do jogo. 
     */
    static orxSTATUS orxFASTCALL Run();

protected:
    gGame();
    ~gGame();

private:
    static gGame * instance;
    gStatus * status;
};

#endif // __G_GAME_H__
