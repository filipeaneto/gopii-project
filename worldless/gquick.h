/**
 * @file  gquick.h
 * @brief  Declaração da classe do módulo gQuick.
 * @author  Filipe Neto
 * @version 1.0
 * @date  Criação: 04/01/2012
 * @date  Alteração: 04/01/2012
 * @todo 
 *  @li Proteger o início e saída indevida.
 *  @li Pause e resume
 *
 *  Este arquivo contém a declaração da classe do módulo gQuick.
 */

#ifndef __G_QUICK__
#define __G_QUICK__

#include <list>

#include "orx.h"
#include "glabel.h"
#include "genemy.h"
#include "gmodule.h"
#include "gplayer.h"

using namespace std;

/**
 * @class gQuick
 * @author Filipe Neto
 * @date 03/25/2012
 * @file gquick.h
 * @brief 
 */
class gQuick : public gModule
{

public:
    static gQuick * Instance();

    static orxSTATUS orxFASTCALL EventHandler(const orxEVENT* _pEvent);

    static void orxFASTCALL
    Update(const orxCLOCK_INFO * _pstClockInfo, void * _pstContext);

    orxSTATUS orxFASTCALL Init(gStatus ** _status);
    orxSTATUS orxFASTCALL Exit();
    orxBOOL   IsRunning();
    orxBOOL   IsPaused();
    orxBOOL   Resume();
    orxBOOL   Pause();

protected:
    gQuick();
    ~gQuick();
    
    static void Collision(gObject * _object1, gObject * _object2);
    
    void UpdateScore(orxFLOAT * _pfTick);
    void UpdateEnemy(orxFLOAT * _pfTick);
    void UpdateItem(orxFLOAT * _pfTick);
    void UpdateDecoration(orxFLOAT * _pfTick);

private:
    const orxSTRING zSectionName;
    const orxSTRING zAreaSection;
    
    orxAABOX stScreen;

    static gQuick * instance;
    gStatus       * status;
    gPlayer       * player;
    list<gEnemy*> *  enemies;

    orxVIEWPORT * pstViewport;
    orxCLOCK    * pstClock;

    gLabel * life;
    gLabel * score;

    orxFLOAT fScreenWidth;
    orxFLOAT fScreenHeight;

    orxU32 u32Area;
    orxU32 u32Score;
};

#endif // __G_QUICK__
