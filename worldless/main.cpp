/**
 * @mainpage Gopii Worldless
 *
 * @section intro_sec Introdução
 *
 * Introdução do Jogo aqui!
 */

#include "ggame.h"

int main(int argc, char *argv[])
{ 
	orx_Execute(argc, argv, gGame::Init, gGame::Run, gGame::Exit);
}
