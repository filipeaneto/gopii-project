/**
 * @file  ggame.cpp
 * @brief  Implementação dos métodos da classe gGame.
 * @author  Filipe Neto
 * @version 1.0
 * @date  Criação: 03/25/2012
 * @date  Alteração: 03/25/2012
 * @todo 
 *  @li Suporte para pausar e resumir os módulos.
 *
 *  Este arquivo contém a implementação dos métodos da class gGame.
 */

#include "ggame.h"

gGame * gGame::instance = 0; 

orxSTATUS orxFASTCALL gGame::EventHandler(const orxEVENT* _pEvent)
{
    orxSTATUS eResult = orxSTATUS_SUCCESS;

    switch(_pEvent->eType)
    {
    case orxEVENT_TYPE_USER_DEFINED:
    {
        switch(_pEvent->eID)
        {
        case gEVENT_INIT_MODULE:
        {
            gModule * module = (gModule*)(_pEvent->pstPayload);
            module->Init(&(Instance()->status));

            break;
        }

        case gEVENT_EXIT_MODULE:
        {
            gModule * module = (gModule *)(_pEvent->pstPayload);
            module->Exit();

            break;
        }

        default:
        {
            // Nao encontrou o evento enviado, orxSTATUS_FAILURE forca a
            // procurar nos outros handles disponiveis
            eResult = orxSTATUS_FAILURE;
            break;
        }
        }
    }
    default:
    {
        eResult = orxSTATUS_FAILURE;
    }
    }

    return eResult;
}

void gGame::Exit()
{
    delete instance;

    orxLOG("Jogo finalizado com sucesso!");
}

orxSTATUS orxFASTCALL gGame::Init()
{
    orxEvent_AddHandler(orxEVENT_TYPE_USER_DEFINED, EventHandler);

    if(orxConfig_PushSection("Game"))
    {
        // Antes de ser carregado qualquer estado de usuario, sao carregadas
        // algumas configuracoes padrao
        Instance()->status = new gStatus(orxConfig_GetString("DefaultSave"));
        Instance()->status->zSaveFile = "./config/save/main.ini";
        Instance()->status->zPlayer = orxConfig_GetListString("PlayerList", 0);
    }

    // Envia o sinal para o inicio do modulo
    orxEVENT_SEND(orxEVENT_TYPE_USER_DEFINED,
                  gEVENT_INIT_MODULE,
                  orxNULL, orxNULL,
                  // Modulo inicial
                  gQuick::Instance());

    return orxSTATUS_SUCCESS;
}

gGame * gGame::Instance()
{
    if(instance == 0)
    {
        instance = new gGame();
    }

    return instance;
}

orxSTATUS orxFASTCALL gGame::Run()
{
    return orxSTATUS_SUCCESS;
}

gGame::gGame() :
    status(0)
{

}

gGame::~gGame()
{
    delete status;

    instance = 0;
}
