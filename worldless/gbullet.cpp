/**
 * @file  gbullet.cpp
 * @brief  Implementação dos métodos da classe gBullet.
 * @author  Filipe Neto
 * @version 1.0
 * @date  Criação: 04/01/2012
 * @date  Alteração: 04/08/2012
 * @todo
 *  @li Muitas coisas :(
 *
 *  Este arquivo contém a implementação dos métodos da classe gBullet.
 */

#include "gbullet.h"

gBullet::gBullet(const orxSTRING _zSectionName, const gObject * _owner) :
    gObject(_zSectionName, gOBJECT_BULLET),
    owner(_owner),
    bIsDead(orxFALSE)
{
    if(orxConfig_PushSection(zSectionName))
    {
        orxVECTOR vPos;
        
        u32Damage = orxConfig_GetU32("Damage");

        fLifeTime = orxConfig_GetFloat("LifeTime");

        zCollisionFX = orxConfig_GetString("CollisionFX");

        // Por segurança
        orxObject_SetLifeTime(pstObject, -1.0f);

        if(fLifeTime > 0.0f)
        {
            dEndTime = orxSystem_GetTime() + fLifeTime;
        }
        
        // Posicionar
        owner->GetPosition(&vPos);
        SetPosition(&vPos);

        orxConfig_PopSection();
    }
}

gBullet::~gBullet()
{

}

void gBullet::Update()
{
    if(fLifeTime > 0.0f)
    {
        if(dEndTime < orxSystem_GetTime())
        {
            bIsDead = orxTRUE;
        }
    }
}


void gBullet::Collision()
{
    bIsDead = orxTRUE;
}

orxU32 gBullet::GetDamage() const
{
    return u32Damage;
}

void gBullet::Select()
{
}

orxBOOL gBullet::IsDead()
{
    return bIsDead;
}
