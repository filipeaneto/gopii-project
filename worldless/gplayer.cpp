/**
 * @file  
 * @brief  
 * @author Filipe Neto
 * @version 1.0
 * @date  Criação: 03/25/2012
 * @date  Alteração: 04/08/2012
 * @todo 
 *
 */
 
#include "gplayer.h"

gPlayer::gPlayer(const orxSTRING _zSectionName) :
    gObject(_zSectionName, gOBJECT_PLAYER),
    u32Level(0),
    u32Experience(0),
    bIsDead(orxFALSE)
{
    if(orxConfig_PushSection(zSectionName))
    {
        // Inicializa a experiencia necessaria e o tiro baseado no nivel
        u32ExpRequired = orxConfig_GetListU32("ExpRequiredList", u32Level);
        zBulletSection = orxConfig_GetListString("BulletList", u32Level);

        // Recupera valores de vida e velocidade
        u32Life = orxConfig_GetU32("Life");
        fSpeed = orxConfig_GetFloat("Speed");

        // Recupera efeitos
        zIdleFX = orxConfig_GetString("IdleFX");
        zMovingFX = orxConfig_GetString("MovingFX");

        pstClock = orxClock_CreateFromConfig(orxConfig_GetString("Clock"));

        orxClock_Register(pstClock, Update, this,
                          orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);

        orxConfig_PopSection();
    }
}

gPlayer::~gPlayer()
{
    orxClock_Unregister(pstClock, Update);
    orxClock_Delete(pstClock);
}

void gPlayer::Select()
{
    // Nao eh selecionavel
}

void orxFASTCALL
gPlayer::Update(const orxCLOCK_INFO * _pstClockInfo, void * _pstContext)
{
    static orxFLOAT fBulletTick = 0.0f;
    
    fBulletTick += _pstClockInfo->fDT;
    
    gPlayer * player = (gPlayer *)(_pstContext);

    // Se o clique esta ativo, jogador se movimenta
    if(orxInput_IsActive("Click"))
    {
        player->Moving();
        // Senao, espera
    }
    else
    {
        player->Idle();
    }
    
    if(orxConfig_PushSection(player->zBulletSection))
    {
        if(fBulletTick > 1.0f / orxConfig_GetFloat("Frequency"))
        {
            player->
            bullets.push_back(new gBullet(player->zBulletSection, player));
            
            fBulletTick = 0.0f;
        }
        
        orxConfig_PopSection();
    }
    
    list<gBullet *>::iterator it = player->bullets.begin(), tmp;
    
    while(it != player->bullets.end())
    {
        if((*it)->IsDead())
        {
            tmp = it;
            it++;
            player->bullets.erase(tmp);
            delete *tmp;
        }
        else
        {
            (*it)->Update();
            it++;
        }
    }
}

orxU32 gPlayer::GetLife() const
{
    return u32Life;
}

void gPlayer::Idle()
{
    orxObject_SetSpeed(pstObject, &(orxVECTOR_0));
    orxObject_SetTargetAnim(pstObject, zIdleFX);
}

void gPlayer::Moving()
{
    orxVECTOR vMPos, vPPos, vSpeed;

    orxObject_GetPosition(pstObject, &vPPos);
    orxVector_Set(&vPPos, vPPos.fX, vPPos.fY + 24, vPPos.fZ);
    orxRender_GetWorldPosition(orxMouse_GetPosition(&vMPos), &vMPos);
    orxVector_Sub(&vSpeed, &vMPos, &vPPos);

    orxVector_Mulf(&vSpeed, &vSpeed, fSpeed);

    // Vetor vSpeed eh um vetor na direcao do clique com modulo baseado na
    // distancia entre o mouse e o player multiplicado pela velocidade do player
    // O fator de correcao (+24) impede o dedo ou o ponteiro do mouse de
    // sobrepor a imagem do objeto
    orxObject_SetSpeed(pstObject, &vSpeed);

    orxObject_SetTargetAnim(pstObject, zMovingFX);
}

void gPlayer::AddExperience(orxU32 _u32Exp)
{
    u32Experience += _u32Exp; 
}

void gPlayer::AddLife(orxU32 _u32Life)
{
    u32Life += _u32Life;
}

void gPlayer::DecLife()
{
    if(u32Life != 0)
        u32Life--;
    else
        bIsDead = orxTRUE;
}

