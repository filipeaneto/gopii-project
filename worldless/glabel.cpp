/**
 * @file  glabel.cpp
 * @brief  Implementação dos métodos da classe gLabel.
 * @author  Filipe Neto
 * @version 1.0
 * @date  Criação: 04/01/2012
 * @date  Alteração: 04/01/2012
 * @todo
 *  @li Fazer o construtor.
 *
 *  Este arquivo contém a implementação dos métodos da class gLabel.
 */

#include "glabel.h"

gLabel::~gLabel()
{
}

gLabel::gLabel(const orxSTRING _zObjectSection, const orxSTRING _zString) :
    gObject(_zObjectSection, gOBJECT_LABEL)
{
    orxObject_SetTextString(pstObject, _zString);
}

void gLabel::Select()
{
}

orxSTATUS gLabel::SetString(const orxSTRING _zString)
{
    return orxObject_SetTextString(pstObject, _zString);
}

