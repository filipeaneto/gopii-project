/**
 * @file  gmodule.h
 * @brief  Declaração da classe abstrata dos módulos.
 * @author  Filipe Neto
 * @version 1.0
 * @date  Criação: 04/01/2012
 * @date  Alteração: 04/01/2012
 * @todo 
 *  @li Nada :)
 *
 *  Este arquivo contém a declaração da classe \b abstrata gModule que 
 *  define as principais funções de um módulo.
 */

#ifndef __G_MODULE__
#define __G_MODULE__

#include "orx.h"
#include "gstatus.h"

#define gEVENT_INIT_MODULE 0
#define gEVENT_EXIT_MODULE 1

/**
 * @class gModule
 * @author Filipe Neto
 * @date 03/25/2012
 * @file gmodule.h
 * @brief 
 */
class gModule
{
public:
    virtual orxSTATUS orxFASTCALL Init(gStatus ** _status) = 0;
    virtual orxSTATUS orxFASTCALL Exit() = 0;
    virtual orxBOOL   IsRunning() = 0;
    virtual orxBOOL   IsPaused() = 0;
    virtual orxBOOL   Resume() = 0;
    virtual orxBOOL   Pause() = 0;
};

#endif // __G_MODULE__
