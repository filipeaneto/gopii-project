/**
 * @file  gbullet.h
 * @brief  Declaração da classe gBullet.
 * @author  Filipe Neto
 * @version 1.0
 * @date  Criação: 04/01/2012
 * @date  Alteração: 04/08/2012
 * @todo 
 *  @li Muitas coisas :(
 *
 *  Este arquivo contém a declaração da classe gBullet.
 */
 
#ifndef __G_BULLET_H__
#define __G_BULLET_H__

#include "orx.h"
#include "gobject.h"

/**
 * @class gBullet
 * @author Filipe Neto
 * @date 03/25/2012
 * @file gbullet.h
 * @brief Classe para bullets de enemies e players.
 */
class gBullet : public gObject
{
public:
    /**
     * @brief Se uma bullet é selecionada, nada acontece. 
     */
    void Select();

    /**
     * @brief Responsável por "autodestruição" da bullet. Algumas bullets podem 
     * conter algum comportamento exclusivo. Futuramente estes comportamentos 
     * podem ser definidos aqui.
     */
    void Update();

    /**
     * @brief Construtor padrão.
     * @param _zSectionName Nome da seção que contém a bullet.
     * @param _owner Objecto que gerou a bullet (gPlayer ou gEnemy).
     */
    gBullet(const orxSTRING _zSectionName, const gObject * _owner);
    ~gBullet();

    /**
     * @brief Retorna dano da bullet.
     * @return Valor do dano.
     */
    orxU32 GetDamage() const;

    /**
     * @brief Chamado quando uma colisão acontece.
     */
    void Collision();
    
    orxBOOL IsDead();

private:
    orxU32   u32Damage;
    orxFLOAT fLifeTime;
    
    orxDOUBLE dEndTime;

    orxBOOL bIsDead;

    const orxSTRING zCollisionFX;
    
    const gObject * owner;
};

#endif // __G_BULLET_H__
