/**
 * @file  gquick.cpp
 * @brief  Implementação dos métodos da classe gQuick.
 * @author  Filipe Neto
 * @version 1.0
 * @date  Criação: 04/01/2012
 * @date  Alteração: 04/08/2012
 * @todo
 *  @li Limpar memória no exit.
 *  @li Suportar pause e resume.
 *
 *  Este arquivo contém a implementação dos métodos da classe gGame.
 */

#include "gquick.h"

gQuick * gQuick::instance = 0;

gQuick::gQuick() :
    zSectionName("QuickModule")
{
}

gQuick::~gQuick()
{
}

gQuick * gQuick::Instance()
{
    if(instance == 0)
    {
        instance = new gQuick();
    }

    return instance;
}

void gQuick::Collision(gObject * _object1, gObject * _object2)
{
    orxU32 u32Type = _object1->GetType() | _object2->GetType();
    
    if(u32Type == (gOBJECT_PLAYER | gOBJECT_ENEMY))
    {
        orxCHAR zLife[20];
        gPlayer * player;
        
        if(_object1->GetType() == gOBJECT_PLAYER)
        {
            ((gEnemy *)(_object2))->Kill();
            player = (gPlayer *)(_object1);
        }
        else
        {
            ((gEnemy *)(_object1))->Kill();
            player = (gPlayer *)(_object2);
        }
        
        player->DecLife();
        orxString_Print(zLife, "Life: %.2d", player->GetLife());
        Instance()->life->SetString(zLife);
    }
    else if(u32Type == (gOBJECT_BULLET | gOBJECT_ENEMY))
    {
        if(_object1->GetType() == gOBJECT_BULLET)
        {
            ((gEnemy *)(_object2))->Kill();
            ((gBullet *)(_object1))->Collision();
        }
        else
        {
            ((gEnemy *)(_object1))->Kill();
            ((gBullet *)(_object2))->Collision();
        }
        
        Instance()->u32Score += 10;
    }
}

orxSTATUS orxFASTCALL gQuick::EventHandler(const orxEVENT* _pstEvent)
{
    orxSTATUS eResult = orxSTATUS_SUCCESS;

    switch(_pstEvent->eType)
    {
    case orxEVENT_TYPE_PHYSICS:
    {
        if(_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD)
        {
            
            orxOBJECT * pstObject1 = orxOBJECT(_pstEvent->hSender);
            orxOBJECT * pstObject2 = orxOBJECT(_pstEvent->hRecipient);
            gObject * object1 = (gObject *)(orxObject_GetUserData(pstObject1));
            gObject * object2 = (gObject *)(orxObject_GetUserData(pstObject2));

            if(object1 == 0 || object2 == 0)
                return orxSTATUS_SUCCESS;
                
            Collision(object1, object2);
        }

        break;
    }
    default:
    {
        eResult = orxSTATUS_FAILURE;
    }
    }

    return eResult;
}

void gQuick::UpdateScore(orxFLOAT * _pfTick)
{
    if(*_pfTick > 1.0f)
    {
        *_pfTick = *_pfTick - 1.0f;

        orxCHAR zScore[20];
        orxU32 u32Points = 2 + 2 * Instance()->u32Area;
        Instance()->u32Score += u32Points;
        Instance()->player->AddExperience(u32Points);

        orxString_Print(zScore, "Score: %.5d", Instance()->u32Score);
        Instance()->score->SetString(zScore);
    }
}

void gQuick::UpdateEnemy(orxFLOAT * _pfTick)
{
    list<gEnemy *>::iterator it = enemies->begin(), tmp;
    
    while(it != enemies->end())
    {
        if((*it)->IsDead())
        {
            tmp = it;
            it++;
            enemies->erase(tmp);
            delete *tmp;
        }
        else
        {
            (*it)->Update();
            it++;
        }
    }
    
    if(*_pfTick > 1.0f / orxConfig_GetFloat("EnemyFrequency"))
    {
        *_pfTick = *_pfTick - 1.0f / orxConfig_GetFloat("EnemyFrequency");

        if(orxMath_GetRandomFloat(.0f, .99f) < orxConfig_GetFloat("EnemyRate"))
        {
            orxVECTOR vSpeed;

            gEnemy * enemy = new gEnemy(orxConfig_GetString("Enemy"),
                                        &stScreen);

            enemy->GetSpeed(&vSpeed);
            orxVector_Set(&vSpeed, vSpeed.fX,
                          vSpeed.fY + orxConfig_GetFloat("Speed"),
                          vSpeed.fZ);
            enemy->SetSpeed(&vSpeed);
            
            enemies->push_back(enemy);
        }
    }
}

void gQuick::UpdateItem(orxFLOAT * _pfTick)
{

}

void gQuick::UpdateDecoration(orxFLOAT * _pfTick)
{

}

void orxFASTCALL
gQuick::Update(const orxCLOCK_INFO * _pstClockInfo, void * _pstContext)
{
    static orxFLOAT fTickItem       = 0.0f;
    static orxFLOAT fTickEnemy      = 0.0f;
    static orxFLOAT fTickDecoration = 0.0f;
    static orxFLOAT fTickScore      = 0.0f;

    fTickItem       += _pstClockInfo->fDT;
    fTickScore      += _pstClockInfo->fDT;
    fTickEnemy      += _pstClockInfo->fDT;
    fTickDecoration += _pstClockInfo->fDT;

    if(orxConfig_PushSection(Instance()->zAreaSection))
    {
        Instance()->UpdateItem(&fTickItem);
        Instance()->UpdateEnemy(&fTickEnemy);
        Instance()->UpdateDecoration(&fTickDecoration);

        orxConfig_PopSection();
    }

    Instance()->UpdateScore(&fTickScore);
}

orxSTATUS orxFASTCALL gQuick::Init(gStatus ** _status)
{
    status = *_status;

    if(orxConfig_PushSection(zSectionName))
    {
        orxVECTOR vTL, vBR;
        orxCHAR zLife[20];
        orxCHAR zScore[20];

        // Inicializa o Player
        player = new gPlayer(status->zPlayer);
        
        enemies = new list<gEnemy *>();

        // Recupera tamanho da tela
        orxDisplay_GetScreenSize(&fScreenWidth, &fScreenHeight);

        // Cria viewport e clock
        pstViewport = orxViewport_CreateFromConfig(orxConfig_GetString("Viewport"));
        pstClock = orxClock_CreateFromConfig(orxConfig_GetString("Clock"));

        // Cria score
        u32Score = 0;
        orxString_Print(zScore, "Score: %.5d", u32Score);
        score = new gLabel(orxConfig_GetString("Score"), zScore);
        score->SetPosition(fScreenWidth/2, fScreenHeight/2);

        // Cria life
        orxString_Print(zLife, "Life: %.2d", player->GetLife());
        life = new gLabel(orxConfig_GetString("Life"), zLife);
        life->SetPosition(-fScreenWidth/2, fScreenHeight/2);

        // Inicializa a caixa da tela
        orxVector_Set(&vTL, -fScreenWidth/2, -fScreenHeight/2, 0.0f);
        orxVector_Set(&vBR,  fScreenWidth/2,  fScreenHeight/2, 0.0f);
        orxAABox_Set(&stScreen, &vTL, &vBR);

        // Inicializa a Area
        u32Area  = 0;
        zAreaSection = orxConfig_GetListString("AreaList", u32Area);

        orxClock_Register(pstClock, Update, orxNULL,
                          orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);

        orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, EventHandler);

        orxConfig_PopSection();
    }

    return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL gQuick::Exit()
{
    orxClock_Unregister(pstClock, Update);
    orxClock_Delete(pstClock);

    delete life;
    delete score;

    delete player;
    
    // TODO deletar todos enemies

    orxViewport_Delete(pstViewport);

    return orxSTATUS_SUCCESS;
}

orxBOOL gQuick::IsPaused()
{
}

orxBOOL gQuick::IsRunning()
{
}

orxBOOL gQuick::Pause()
{
}

orxBOOL gQuick::Resume()
{
}
