/**
 * @file  
 * @brief  
 * @author Filipe Neto
 * @version 1.0
 * @date  Criação: 04/01/2012
 * @date  Alteração: 04/08/2012
 * @todo 
 *
 */

#ifndef __G_ENEMY__
#define __G_ENEMY__

#include "orx.h"
#include "gobject.h"

class gEnemy : public gObject
{
public:
    void Select();

    void Update();

    gEnemy(const orxSTRING _zSectionName, orxAABOX * _pstScreen);
    ~gEnemy();

    void Kill();
    orxBOOL IsDead();

private:
    orxAABOX * pstScreen;

    orxU32 u32Life;
    orxFLOAT fLifeTime;
    
    orxBOOL bDieOutOfScreen;
    orxBOOL bIsDead;
};

#endif // __G_ENEMY__
