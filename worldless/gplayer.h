/**
 * @file  
 * @brief  
 * @author Filipe Neto
 * @version 1.0
 * @date  Criação: 03/25/2012
 * @date  Alteração: 04/08/2012
 * @todo 
 *
 */

#ifndef __G_PLAYER_H__
#define __G_PLAYER_H__

#include <list>

#include "gobject.h"
#include "gbullet.h"

using namespace std;

/**
 * @class gPlayer
 * @author Filipe Neto
 * @date 03/25/2012
 * @file gplayer.h
 * @brief 
 */
class gPlayer : public gObject
{
public:
    void Select();

    static void orxFASTCALL
    Update(const orxCLOCK_INFO * _pstClockInfo, void * _pstContext);

    gPlayer(const orxSTRING _zSectionName);
    ~gPlayer();

    orxU32 GetLife() const;
    
    void AddExperience(orxU32 _u32Exp);
    void AddLife(orxU32 _u32Life);
    void DecLife();

protected:
    void Moving();
    void Idle();

private:
    orxCLOCK * pstClock;
    
    list<gBullet *> bullets;
    
    const orxSTRING zBulletSection;
    
    const orxSTRING zIdleFX;
    const orxSTRING zMovingFX;
    
    orxFLOAT fSpeed;
    
    orxU32 u32Level;
    orxU32 u32Experience;
    orxU32 u32ExpRequired;
    orxU32 u32Life;
    
    orxBOOL bIsDead;
};

#endif // __G_PLAYER_H__
