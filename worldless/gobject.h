/**
 * @file  gobject.h
 * @brief  Declaração da classe gObject.
 * @author  Filipe Neto
 * @version 1.0
 * @date  Criação: 04/01/2012
 * @date  Alteração: 04/01/2012
 * @todo 
 *  @li Alguns getters and setter.
 *
 *  Este arquivo contém a declaração da classe gObject.
 */
 
#ifndef __G_OBJECT_H__
#define __G_OBJECT_H__

#include "orx.h"

#define gOBJECT_PLAYER 1
#define gOBJECT_ENEMY  2
#define gOBJECT_BULLET 4
#define gOBJECT_LABEL  8

/**
 * @class gObject
 * @author Filipe Neto
 * @date 03/25/2012
 * @file gobject.h
 * @brief 
 */
class gObject
{
public:
    virtual void Select() = 0;

    virtual orxSTATUS SetPosition(const orxVECTOR * _pvPosition);
    virtual orxSTATUS SetPosition(orxFLOAT fX, orxFLOAT fY, orxFLOAT fZ = 0.0f);
    
    virtual orxSTATUS SetPivot(const orxVECTOR * _pvPivot);
    virtual orxSTATUS SetSpeed(const orxVECTOR * _pvSpeed);

    virtual orxSTATUS GetPosition(orxVECTOR * _pvPosition) const;
    virtual orxSTATUS GetSize(orxVECTOR * _pvSize) const;
    virtual orxSTATUS GetSpeed(orxVECTOR * _pvSpeed) const;
    
    virtual orxU32    GetType() const;

    gObject(const orxSTRING _zSectionName, orxU32 _u32Type);
    virtual ~gObject();

protected:
    orxOBJECT * pstObject;
    const orxSTRING zSectionName;
    
private:
    orxU32 u32Type;
};

#endif // __G_OBJECT_H__
