/**
 * @file  
 * @brief  
 * @author Filipe Neto
 * @version 1.0
 * @date  Criação: 03/25/2012
 * @date  Alteração: 04/08/2012
 * @todo 
 *
 */
 
#include "gobject.h"

orxSTATUS gObject::GetSize(orxVECTOR * _pvSize) const
{
    if(orxObject_GetSize(pstObject, _pvSize)) {
        return orxSTATUS_SUCCESS;
    }

    return orxSTATUS_FAILURE;
}

orxSTATUS gObject::SetPivot(const orxVECTOR * _pvPivot)
{
    return orxObject_SetPivot(pstObject, _pvPivot);
}

orxSTATUS gObject::SetPosition(const orxVECTOR * _pvPosition)
{
    return orxObject_SetPosition(pstObject, _pvPosition);
}

gObject::gObject(const orxSTRING _zSectionName, orxU32 _u32Type) :
    zSectionName(_zSectionName),
    u32Type(_u32Type)
{
    if(orxConfig_PushSection(zSectionName)) {
        pstObject = orxObject_CreateFromConfig(orxConfig_GetString("Object"));
        
        // Armazenar a referencia do gObject permite recupera-la em eventos
        // que o orxOBJECT participe
        orxObject_SetUserData(pstObject, this);

        // Os filhos devem referenciar o mesmo gObject
        // Não sei se está funcionando, estava sem internet :(
        orxOBJECT * pstChild = orxObject_GetChild(pstObject);
        while(pstChild != orxNULL)
        {
            orxObject_SetUserData(pstChild, this);
            pstChild  = orxObject_GetSibling(pstChild);
        }

        orxConfig_PopSection();
    }
}

gObject::~gObject()
{
    orxObject_SetUserData(pstObject, orxNULL);
    // Esta funcao eh utilizada ao inves de orxObject_Delete por ser mais segura
    orxObject_SetLifeTime(pstObject, orxFLOAT_0);
}

orxSTATUS gObject::GetPosition(orxVECTOR * _pvPosition) const
{
    if(orxObject_GetPosition(pstObject, _pvPosition)) {
        return orxSTATUS_SUCCESS;
    }

    return orxSTATUS_FAILURE;
}

orxU32 gObject::GetType() const
{
    return u32Type;
}

orxSTATUS gObject::SetPosition(orxFLOAT fX, orxFLOAT fY, orxFLOAT fZ)
{
    orxVECTOR vPos;
    orxVector_Set(&vPos, fX, fY, fZ);
    return SetPosition(&vPos);
}

orxSTATUS gObject::GetSpeed(orxVECTOR * _pvSpeed) const
{
    if(orxObject_GetSpeed(pstObject, _pvSpeed)) {
        return orxSTATUS_SUCCESS;
    }

    return orxSTATUS_FAILURE;
}

orxSTATUS gObject::SetSpeed(const orxVECTOR* _pvSpeed)
{
    return orxObject_SetSpeed(pstObject, _pvSpeed);
}

